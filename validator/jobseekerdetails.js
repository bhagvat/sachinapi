module.exports = {
    "jobseeker.id": { isString: true, notEmpty: false },
    "jobseeker.address": { notEmpty: true, isString: true },
    "jobseeker.pincode": { notEmpty: true, isString: true },
    "jobseeker.skill": { notEmpty: true, isString: true }
};