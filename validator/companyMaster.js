module.exports = {
    "company.name": { isString: true, notEmpty: false },
    "company.description": { isString: true, notEmpty: false }
};

// "device_id":"mobile123"
// "Company_id":"nokia123"
// "name":"nokia"
// "description":"company descroption"
//"status"