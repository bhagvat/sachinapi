module.exports = {
    "jobseeker.username": { isString: true, notEmpty: false },
    "jobseeker.password": { notEmpty: true, isString: true },
    "jobseeker.email": { notEmpty: true, isEmail: true },
    "jobseeker.mobile": { notEmpty: true, isString: true }
};