module.exports = {
    "device.type": { isString: true, notEmpty: false },
    "device.description": { isString: true, notEmpty: false }
};

// "device_id":"mobile123"
// "type":"mobile",
// "description":"all mobile devices"
// "added date":"12/2/2017"
//status