module.exports = {
    "admin.username": { isString: true, notEmpty: false },
    "admin.password": { notEmpty: true, isString: true },
    "admin.firstName": { isString: true, notEmpty: false },
    "admin.lastName": { isString: true, notEmpty: false },
    "admin.email": { notEmpty: true, isEmail: true },
    "admin.mobile": { notEmpty: true, isString: true }
};