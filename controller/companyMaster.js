var jwt = require("jsonwebtoken");
var ObjectId = require('mongodb').ObjectID;
var companyValidator = require('../validator/companyMaster.js');



module.exports = function(db) {
    return {
        addDeviceCompany: function(req, res) {
            var companyData = req.body.company;

            db.collection('CompanyMaster').insertOne(companyData, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                } else {
                    res.send({
                        err: false,
                        result: "device company added succesfully done"
                    });

                }
            });
        },

        getAllCompanies: function(req, res) {
            // var id = req.params.id;
            db.collection('CompanyMaster').find({}, { name: 1 }).toArray(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "admin with given id not found"
                    });
                } else {
                    return res.send({
                        err: false,
                        company: result
                    });

                }
            });
        }
    };
};