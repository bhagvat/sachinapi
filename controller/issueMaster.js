var jwt = require("jsonwebtoken");
var ObjectId = require('mongodb').ObjectID;


module.exports = function(db) {
    return {
        addModelIssue: function(req, res) {
            var issuedata = req.body.issue;
            issuedata.modelId = req.params.id;
            db.collection('Issue_Master').insertOne(issuedata, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                } else {
                    res.send({
                        err: false,
                        result: "issue added succesfully done"
                    });

                }
            });
        },

        getIssuesByModel: function(req, res) {

            db.collection('Issue_Master').find({ "modelId": req.params.id }).toArray(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "issue with given id not found"
                    });
                } else {
                    return res.send({
                        err: false,
                        issues: result
                    });

                }
            });
        }
    };
};