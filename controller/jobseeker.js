var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');
var password = require("../helper/password.js");
var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../helper/sendMail.js');
var send_sms = require('../helper/send_sms.js');
var send_verify_sms = require('../helper/send_sms_verification.js');
var jobSeekerValidator = require('../validator/jobseeker.js');
var jobSeekerDetalisValidator = require('../validator/jobseekerdetails.js');

module.exports = function(db) {
    return {
        addjobseeker: function(req, res) {
            // req.checkBody(jobSeekerValidator);
            // if (!req.validateAndRespond()) return;
            var userData = req.body.jobseeker;
            console.log("password" + userData.password);

            password.hashPassword(userData.password, function(_password) {
                console.log("here.....");
                console.log(_password);
                var _token = userData.email + userData.password;
                userData.token = jwt.sign(_token, (new Date()).toString());
                userData.password = _password;
                console.log("encrytp assword:" + userData.password);
                var datenow = new Date();
                userData.createdOn = datenow;
                db.collection('JobSeekerMaster').insertOne(userData, function(err, result) {
                    console.log(err);
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        send_verify_sms.sendVerifySMS(userData.mobile);
                        res.send({
                            err: false,
                            result: "shortely we willl recive an confirmation msg"
                        });
                    }
                });
            });
        },
        jobseekerLogin: function(req, res) {
            req.checkBody({
                'jobseeker.password': {
                    notEmpty: true
                },
                'jobseeker.email': {
                    notEmpty: true
                }
            });
            if (!req.validateAndRespond()) return;
            var email = req.body.jobseeker.email;
            db.collection('JobSeekerMaster').findOne({ email: email }, function(err, jobseeker) {
                if (jobseeker === null) {
                    return res.send({
                        err: true,
                        error: ["email doesn't exist"]
                    });
                } else {
                    bcrypt.compare(req.body.jobseeker.password, jobseeker.password, function(err, result) {
                        console.log("Bcrypt returns: " + result);
                        if (err) {
                            res.status(501).json({
                                err: false,
                                error: ["Server error"]
                            });
                        } else {
                            if (result) {
                                var _token = req.body.jobseeker.email + req.body.jobseeker.password;
                                var token = jwt.sign(_token, (new Date()).toString());
                                db.collection('JobSeekerMaster').update({ _id: new ObjectId(jobseeker._id) }, { $set: { "token": token } }, function(err, result) {
                                    if (err) {
                                        return res.send({
                                            err: true,
                                            error: err
                                        });
                                    } else {
                                        res.json({
                                            err: false,
                                            result: { token: token }
                                        });
                                    }
                                });
                            } else {
                                res.status(401).json({
                                    err: true,
                                    error: ["Invalid email/password combination"]
                                });
                            }
                        }
                    });
                }
            });
        },
        jobseekerLogout: function(req, res) {
            if (!req.validateAndRespond()) return;
            var id = req.params.id;
            db.collection('JobSeekerMaster').findOne({ _id: new ObjectId(id) }, function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "JobSeeker with given id not found"
                    });
                } else {
                    db.collection('JobSeekerMaster').update({ _id: new ObjectId(id) }, { $unset: { "token": "" } }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: "JobSeeker logged out successfully."
                            });
                        }
                    });
                }
            });
        },
        //forget password by send opt over mobile and email 
        forgotpassword: function(req, res) {
            var userData = req.body;
            ///OTP///
            var speakeasy = require('speakeasy');
            var secret = speakeasy.generateSecret({ length: 20 });

            var otp = speakeasy.totp({
                secret: secret.base32,
                encoding: 'base32'
            });
            userData.otp = otp;
            ///
            if ("email" == userData.type) {
                db.collection('JobSeekerMaster').update({ "email": userData.identity }, { $set: { "otp": otp } }, function(err, result) {
                    sendMail.Send_Email(userData.identity, 'Forgot Password', 'email_body_forgot', userData).then(function(data) {
                        res.send({
                            err: false,
                            result: data
                        });
                    });
                });
            } else if ("mobile" == userData.type) {
                db.collection('JobSeekerMaster').update({ "mobile": userData.identity }, { $set: { "otp": otp } }, function(err, result) {
                    send_sms.sendSMS(userData.identity, userData.otp).then(function(data) {
                        res.send({
                            err: false,
                            result: data
                        });
                    });
                });
            } else {
                return res.send({
                    err: true,
                    error: "mobile number or email is wrong !"
                });
            }
        },
        ChangePassword: function(req, res) {
            var otp = req.body.otp;
            var Newpassword = req.body.password;
            var conf_password = req.body.confPassword;
            if (Newpassword !== conf_password) {
                return res.send({
                    err: true,
                    error: "Password and confirm password do not match."
                });
            }
            db.collection('JobSeekerMaster').findOne({ "otp": otp }, function(err, result) {
                if (result == null) {
                    return res.send({
                        err: true,
                        error: "Invalid OTP Presented.Please try again"
                    });
                } else {
                    password.hashPassword(Newpassword, function(_password) {
                        db.collection('JobSeekerMaster').update({ "otp": otp }, { $set: { "password": _password } }, function(err, result) {
                            if (err) {
                                return res.send({
                                    err: true,
                                    error: err
                                });
                            } else {
                                return res.send({
                                    err: false,
                                    result: result
                                });
                            }
                        });
                    });

                }
            });
        },
        //add new profile details or update details
        addProfileDetails: function(req, res) {
            req.checkBody(jobSeekerDetalisValidator);
            if (!req.validateAndRespond()) return;
            console.log("@@@@@@" + req.token);
            db.collection('JobSeekerMaster').findOne({ "token": req.token }, { password: 0 }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "Authentication error ..user not found!"
                    });
                } else {
                    var userData = req.body.jobseeker;
                    var datenow = new Date();
                    userData.updatedOn = datenow;
                    db.collection('JobSeekerMaster').update({ "token": req.token }, { $set: userData }, function(err, result) {
                        if (err) {
                            return res.send({
                                err: true,
                                error: err
                            });
                        } else {
                            return res.send({
                                err: false,
                                result: result
                            });
                        }
                    });
                }
            });
        },
        //get jobseeker profile
        getuserProfile: function(req, res) {
            db.collection('JobSeekerMaster').findOne({ "toekn": req.token }, { password: 0, _id: 0 }, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: "job seeker not found with given authentication token"
                    });
                } else {
                    res.send({
                        err: false,
                        result: {
                            user: result
                        }
                    });
                }
            });
        }
    };
};