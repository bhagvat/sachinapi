var jwt = require("jsonwebtoken");
var ObjectId = require('mongodb').ObjectID;
var deviceValidator = require('../validator/deviceMaster.js');

module.exports = function(db) {
    return {
        addDeviceType: function(req, res) {
            var deviceData = req.body.device;
            db.collection('DeviceTypeMaster').insertOne(deviceData, function(err, result) {
                if (err) {
                    return res.send({
                        err: true,
                        error: err
                    });
                } else {
                    res.send({
                        err: false,
                        result: "device type added succesfully done"
                    });

                }
            });
        },

        getAllDeviceTypes: function(req, res) {
            db.collection('DeviceTypeMaster').find({}).toArray(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "admin with given id not found"
                    });
                } else {
                    return res.send({
                        err: false,
                        company: result
                    });

                }
            });
        }
    };
};