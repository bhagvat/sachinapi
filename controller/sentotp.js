var jwt = require("jsonwebtoken");
var bcrypt = require('bcryptjs');
var password = require("../helper/password.js");
var ObjectId = require('mongodb').ObjectID;
var sendMail = require('../helper/sendMail.js');
var send_sms = require('../helper/send_sms.js');
var send_verify_sms = require('../helper/send_sms_verification.js');
var jobSeekerValidator = require('../validator/jobseeker.js');
var jobSeekerDetalisValidator = require('../validator/jobseekerdetails.js');
var randomize = require('randomatic');



module.exports = function(db) {
    return {
        sentotp: function(req, res) {
            debugger
            var mob = req.params.mobile;
            var otp = randomize('0', 4);

            send_sms.sendSMS(mob, otp).then(function(data) {
                res.send({
                    err: false,
                    result: data
                });
            });
        },
        adduser: function(req, res) {
            var userData = req.body.user;

            password.hashPassword(userData.password, function(_password) {
                console.log(_password);
                userData.password = _password;

                db.collection('lifecareusers').insertOne(userData, function(err, result) {
                    console.log(err);
                    if (err) {
                        return res.send({
                            err: true,
                            error: err
                        });
                    } else {
                        res.send({
                            err: false,
                            result: "user added succesfully done"
                        });

                    }
                });
            });
        },


        login: function(req, res) {
            debugger
            var userData = req.body.user;
            var user_name = req.body.user.user_name;

            db.collection('lifecareusers').findOne({ user_name: user_name }, function(err, admin) {
                if (admin === null) {
                    return res.send({
                        err: true,
                        error: ["email doesn't exist"]
                    });
                } else {
                    bcrypt.compare(req.body.user.password, admin.password, function(err, result) {
                        console.log("Bcrypt returns: " + result);
                        if (err) {
                            res.status(501).json({
                                err: true,
                                error: ["Server error or worong password"]
                            });
                        } else {
                            res.json({
                                err: false,
                                result: "user logged in sucessfullu"
                            });
                        }
                    });

                }
            });

        },

        finddonor: function(req, res) {

            var user_type = req.params.user_type;
            var zip_code = req.params.zip_code;

            db.collection('lifecareusers').find({ $and: [{ user_type: user_type }, { zip_code: zip_code }] }).toArray(function(err, result) {
                if (result === null) {
                    return res.send({
                        err: true,
                        error: "admin with given id not found"
                    });
                } else {
                    return res.send({
                        err: false,
                        result: result
                    });
                }
            });


        }
    }
}