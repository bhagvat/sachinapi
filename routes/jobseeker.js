module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'JobSeekerMaster');
    var jobseeker = require('../controller/jobseeker.js')(db);


    app.post('/addJobSeeker', jobseeker.addjobseeker);
    app.post('/jobseeker/login', jobseeker.jobseekerLogin);
    app.post('/jobseeker/:id/logout', jobseeker.jobseekerLogout);
    app.post('/jobseeker/forgotpassword', jobseeker.forgotpassword);
    app.post('/jobseeker/Changepassword', jobseeker.ChangePassword);
    app.put('/jobseeker/addProfileDetails', authentication.ensureAuthentication, jobseeker.addProfileDetails);
    app.get('/jobseeker/getProfile', authentication.ensureAuthentication, jobseeker.getuserProfile);
};