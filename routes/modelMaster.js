module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'admin');
    var model = require('../controller/modelMaster.js')(db);

    app.post('/addCompanyModel/:id', model.addCompanyModel);
    app.get('/getAllCompanyModels/:id', model.getAllCompanyModels);

};