module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'admin');
    var deviceType = require('../controller/deviceMaster.js')(db);

    app.post('/addDeviceType', deviceType.addDeviceType);
    app.get('/getAllDeviceTypes', deviceType.getAllDeviceTypes);

};