module.exports = function(db, app) {
    var deviceCompany = require('../controller/companyMaster.js')(db);

    // app.post('/addDeviceType', authentication.ensureAuthentication, deviceType.addDeviceType);
    // app.get('/getAllDeviceTypes', deviceType.getAllDeviceTypes);

    //id : device type
    app.post('/addDeviceCompany', deviceCompany.addDeviceCompany);
    app.get('/getAllCompanies', deviceCompany.getAllCompanies);


};