module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'admin');
    var adminauth = require('../helper/admin_auth.js')(db, 'admin');


    var admin = require('../controller/admin.js')(db);
    app.post('/addadmin', admin.addAdmin);
    app.post('/admin/login', admin.adminLogin);
    app.post('/admin/:id/logout', admin.adminlogout);
    app.post('/admin/adddevicecompany', authentication.ensureAuthentication, admin.adddevicecompany);
    app.post('/admin/addDevicemodel/:company', authentication.ensureAuthentication, admin.addDevicemodel);
    app.get('/admin/getDeviceNames', admin.getDeviceNames);
    app.get('/admin/getDeviceModelnames/:company', admin.getDeviceModelnames);



    //app.get('/admin/:id', admin.getAdmin);
    //app.post('/admin/:id/activate', admin.adminActivate);
    //app.post('/admin/:id/forgotpassword/email', admin.forgotpassword);
    //app.post('/admin/:id/forgotpassword/sms', admin.forgotpasswordSMS);
    //app.post('/admin/:id/Changepassword', admin.ChangePassword);
    //app.put('/admin/:id', admin.updateAdmin);
    //where sid is the superAdmin id And aid is the admin 
    //app.put('/updatesuperadmin/:id', admin.updatesuperadmin);
    //app.delete('/deleteadmin/:sid/:aid', admin.deleteadmin);
};