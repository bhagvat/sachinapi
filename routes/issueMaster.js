module.exports = function(db, app) {
    var authentication = require('../helper/token_auth.js')(db, 'admin');
    var deviceIssues = require('../controller/issueMaster')(db);

    // app.post('/addDeviceType', authentication.ensureAuthentication, deviceType.addDeviceType);
    // app.get('/getAllDeviceTypes', deviceType.getAllDeviceTypes);

    //id : device type
    app.post('/addIssue/:id', deviceIssues.addModelIssue);
    app.get('/getIssuesByModel/:id', deviceIssues.getIssuesByModel);
};