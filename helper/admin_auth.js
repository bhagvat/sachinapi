
var bcrypt = require('bcryptjs');

function checkHeader(req) {
  var emailHeader = req.headers.email;
  var passwordHeader = req.headers.password;
  if (typeof emailHeader  !== 'undefined' || typeof passwordHeader  !== 'undefined') {
    req.email = emailHeader;
    req.password = passwordHeader;
    return req;
  } else {
    return null;
  }
}

module.exports = function(db, collection) {
  return {
    ensureAuthentication : function (req, res, next) {
      req = checkHeader(req);
      if (req === null) {
        return res.status(403).send({
          result: false,
          err: "email or password not set"
        });
      }
      console.log(req.email);
      console.log(req.password);
      db.collection(collection).findOne({ email: req.email }, function(err,admin)  {
    
        if(err){
          return res.status(500).send({
            result: false,
            err:err 
          });
        }
        else{
           if (admin === null) {
                    return res.send({
                        err: true,
                        error: ["email doesn't exist"]
                    });
                } 
                else{
            bcrypt.compare(req.password, admin.password, function (err, result) {
                        console.log("Bcrypt returns: " + result);
                        if (err) {
                            res.status(501).json({
                                err: false,
                                error: ["Server error"]
                            });
                        } else {
                             if(result){
         // req.user = result;
          next();
        } else {
          res.status(401).json({
            result: false,
            err: "Invalid email or password"
          });
        }
        }
       
      });
    }
        }
  });
  }}}
