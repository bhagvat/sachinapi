// Twilio Credentials 
var account = require('../config/sms_credentials.js');

//require the Twilio module and create a REST client 
var client = require('twilio')(account.accountSid, account.authToken);
var q = require('q');

exports.sendVerifySMS = function(mobNumber) {

    var deferred = q.defer();
    var Mobno = mobNumber;

    client.messages.create({
        to: Mobno,
        from: "+12566662036",
        body: "registration done succesffully...thanks for joining us..make future with us",

    }, function(err, message) {
        if (err) {
            console.log(err);
            deferred.reject(new Error(err));

        } else {
            console.log(message.sid);
            deferred.resolve(message);
        }
    });
    return deferred.promise;
}