// Twilio Credentials 
var account = require('../config/sms_credentials.js');

//require the Twilio module and create a REST client 
var client = require('twilio')(account.accountSid, account.authToken);
var q = require('q');

exports.sendSMS = function(mobNumber, name, registrationdate, surveycompletiondate) {

    var deferred = q.defer();
    var Mobno = mobNumber;

    client.messages.create({
        to: Mobno,
        from: "+12248360669",
        body: "name" + name + "Registration Date" + registrationdate + "Survey Completion Date" +
            surveycompletiondate,

    }, function(err, message) {
        if (err) {
            console.log(err);
            deferred.reject(new Error(err));

        } else {
            console.log(message.sid);
            deferred.resolve(message);
        }
    });
    return deferred.promise;
}