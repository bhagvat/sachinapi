var express = require("express");
var app = express();
require("./config/express.js")(app);
var port = process.env.PORT || 8085;
var cors = require('cors');
app.use(cors());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    next();
});

require("./config/mongo.js")(function(err, db) {
    if (err) throw err;

    //require("./routes/jobseeker.js")(db, app);
    require("./routes/index.js")(db, app);
    require("./routes/companyMaster.js")(db, app);
    require("./routes/deviceMaster.js")(db, app);
    require("./routes/issueMaster.js")(db, app);
    require("./routes/modelMaster.js")(db, app);
    require("./routes/sentotp.js")(db, app);
    app.listen(port);
    console.log("Application listening on port" + port);
});

module.exports = app;